# Django Recycle
Django Recycle is a Django app containing reusable code for Django projects.

## Quick start
Add `django_recycle` to your INSTALLED_APPS setting like this:
```
INSTALLED_APPS = [
    ...
    'django_recycle',
]
```