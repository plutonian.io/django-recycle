from django.apps import AppConfig


class DjangoRecycleConfig(AppConfig):
    name = 'django_recycle'
